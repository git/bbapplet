###############################################################################
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. 
#
# Copyright (C) 2008 Falco Hirschenberger <hirsch@bigfoot.de>
###############################################################################

from PyQt4 import QtCore, QtGui
from ConfigDialogBase import Ui_ConfigDialog

class ConfigDialog(QtGui.QDialog, Ui_ConfigDialog):
  def __init__(self, parent):
    QtGui.QDialog.__init__(self, parent)
    self.setupUi(self)

  def accept(self):
    if self.serverEdit.text().isEmpty() or \
       self.userEdit.text().isEmpty() or \
       self.passEdit.text().isEmpty():
      QtGui.QMessageBox.critical(self, 'Error...', 'Please fill out all entries')
    else:
      QtGui.QDialog.accept(self)



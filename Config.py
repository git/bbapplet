###############################################################################
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. 
#
# Copyright (C) 2008 Falco Hirschenberger <hirsch@bigfoot.de>
###############################################################################

import os, ConfigParser
from PyQt4.QtCore import Qt

# local includes
import ConfigDialog

class Config:
  def __init__(self, parent):
    self.configFile = os.path.expanduser('~/.bbapplet.cfg')
    self.parser = ConfigParser.SafeConfigParser()
    self.dlg = ConfigDialog.ConfigDialog(parent)
    self.config = {'Server': '', 'Port': 0, 'User': '', 'Pass': '', 'ShowFail': False}

    # is it the first start (does the configfile exist)?
    if os.path.exists(self.configFile):
      self.loadConfig()
    else:
      self.showDlg()

  def loadConfig(self):
    self.parser.readfp(open(self.configFile, 'r'))
    self.config['Server'] = self.parser.get('Connection', 'Server')
    self.config['Port'] = self.parser.getint('Connection', 'Port')
    self.config['User'] = self.parser.get('Connection', 'User')
    self.config['Pass'] = self.parser.get('Connection', 'Pass')
    self.config['ShowFail'] = self.parser.getboolean('Notification', 'ShowFail')


  def storeConfig(self):
    self.parser.add_section('Connection')
    self.parser.set('Connection', 'Server', str(self.config['Server']))
    self.parser.set('Connection', 'Port', str(self.config['Port']))
    self.parser.set('Connection', 'User', str(self.config['User']))
    self.parser.set('Connection', 'Pass', str(self.config['Pass']))
    self.parser.add_section('Notification')
    self.parser.set('Notification', 'ShowFail', str(self.config['ShowFail']))
    self.parser.write(open(self.configFile, 'w'))

  def showDlg(self):
    if self.dlg.exec_():
      # we have to convert the QString obejct from the widget to a py-string here
      self.config['Server'] = str(self.dlg.serverEdit.text())
      self.config['Port'] = self.dlg.portSpinBox.value()
      self.config['User'] = str(self.dlg.userEdit.text())
      self.config['Pass'] = str(self.dlg.passEdit.text())
      if self.dlg.showFailBox.checkState() == Qt.Checked: 
        self.config['ShowFail'] = True
      else:
        self.config['ShowFail'] = False

      self.storeConfig()


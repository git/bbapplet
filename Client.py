###############################################################################
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. 
#
# Copyright (C) 2008 Falco Hirschenberger <hirsch@bigfoot.de>
###############################################################################

from threading import Thread
from twisted.spread import pb
from twisted.cred import credentials, error
from twisted.internet import reactor

#local imports
import StatusAction

class Client(Thread):
  def __init__(self, server, port, events="builds"):
    Thread.__init__(self)
    self.server = server
    self.port = port
    self.listener = StatusAction.StatusAction(events)
    
  def run(self):
    self.startConnecting()
    reactor.run(installSignalHandlers=0)

  def startConnecting(self):
    cf = pb.PBClientFactory()
    creds = credentials.UsernamePassword("user", "passwd")
    d = cf.login(creds)
    reactor.connectTCP(self.server, self.port, cf)
    d.addCallbacks(self.connected, self.not_connected)
    return d

  def connected(self, ref):
    ref.notifyOnDisconnect(self.disconnected)
    self.listener.connected(ref)

  def not_connected(self, why):
    if why.check(error.UnauthorizedLogin):
      print """Unable to login.. are you sure we are connecting to a
buildbot.status.client.PBListener port and not to the slaveport?"""
    self.stop()
    return why

  def disconnected(self, ref):
        # we can get here in one of two ways: the buildmaster has
        # disconnected us (probably because it shut itself down), or because
        # we've been SIGINT'ed. In the latter case, our reactor is already
        # shut down, but we have no easy way of detecting that. So protect
        # our attempt to shut down the reactor.
    try:
      self.stop()
    except RuntimeError:
      pass

  def stop(self):
    try:
      reactor.callFromThread(reactor.stop)
    except RuntimeError:
      pass

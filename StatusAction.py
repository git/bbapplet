###############################################################################
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. 
#
# Copyright (C) 2008 Falco Hirschenberger <hirsch@bigfoot.de>
###############################################################################

from twisted.spread import pb

class StatusAction(pb.Referenceable):
  def __init__(self, events):
    self.events = events
    self.builders = list()

  def connected(self, remote):
    print "connected"
    self.remote = remote
    remote.callRemote("subscribe", self.events, 5, self)

  def remote_builderAdded(self, buildername, builder):
    print "builderAdded", buildername
    self.builders.append({'Name': buildername, 'State': '???', 'LastResult': 0})

  def remote_builderRemoved(self, buildername):
    print "builderRemoved", buildername
    for build in self.builders:
      if build["Name"] == buildername:
        self.builders.remove(build)

  def remote_builderChangedState(self, buildername, state, eta):
    print "builderChangedState", buildername, state, eta
    for build in self.builders:
      if build["Name"] == buildername:
        build['State'] = state

  def remote_buildStarted(self, buildername, build):
    print "buildStarted", buildername

  def remote_buildFinished(self, buildername, build, results):
    print "buildFinished", results
    for build in self.builders:
      if build["Name"] == buildername:
        build["LastResult"] = results

  def remote_buildETAUpdate(self, buildername, build, eta):
    print "ETA", buildername, eta

  def remote_stepStarted(self, buildername, build, stepname, step):
    print "stepStarted", buildername, stepname

  def remote_stepFinished(self, buildername, build, stepname, step, results):
    print "stepFinished", buildername, stepname, results

  def remote_stepETAUpdate(self, buildername, build, stepname, step, eta, expectations):
    print "stepETA", buildername, stepname, eta

  def remote_logStarted(self, buildername, build, stepname, step, logname, log):
    print "logStarted", buildername, stepname

  def remote_logFinished(self, buildername, build, stepname, step, logname, log):
    print "logFinished", buildername, stepname

  def remote_logChunk(self, buildername, build, stepname, step, logname, log, channel, text):
    ChunkTypes = ["STDOUT", "STDERR", "HEADER"]
    print "logChunk[%s]: %s" % (ChunkTypes[channel], text)


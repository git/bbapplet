RCC4=pyrcc4
UIC4=pyuic4
RES_FILE=Resources.qrc
UI_FILES=ConfigDialog.ui

all: Resources.py ConfigDialogBase.py

Resources.py: $(RES_FILE)
	$(RCC4) -o $@ $<

ConfigDialogBase.py: $(UI_FILES)
	$(UIC4) -o $@ $<

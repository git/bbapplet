#!/usr/bin/env python
###############################################################################
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. 
#
# Copyright (C) 2008 Falco Hirschenberger <hirsch@bigfoot.de>
###############################################################################

import sys
from PyQt4 import QtCore, QtGui

# local imports
import Config
import Client
import Resources

class BuildbotApplet(QtGui.QWidget):
  def __init__(self):
    QtGui.QWidget.__init__(self)
    if not QtGui.QSystemTrayIcon.isSystemTrayAvailable():
      print "Non system tray available"
      sys.exit(1)
    
    self.config = Config.Config(self)

    self.client = Client.Client(self.config.config['Server'], self.config.config['Port'])
    self.client.start()

    self.okIcon = QtGui.QIcon(":pixmaps/ok.png")
    self.failedIcon = QtGui.QIcon(":pixmaps/no.png")
    self.buildingIcon = QtGui.QIcon(":pixmaps/building.png")
    
    self.trayIcon = QtGui.QSystemTrayIcon()
    self.trayIcon.setIcon(self.okIcon)
    
    self.menu = QtGui.QMenu()
    self.menu.addAction(QtGui.QIcon(":pixmaps/configure.png"), "Configure", self.config.showDlg)
    self.menu.addAction(QtGui.QIcon(":pixmaps/exit.png"), "Quit", self.quit)
    QtCore.QObject.connect(self.trayIcon, QtCore.SIGNAL("activated(QSystemTrayIcon::ActivationReason)"),
                           self.showMenu)
    self.trayIcon.setContextMenu(self.menu)
    self.trayIcon.show()

    self.updateTimer = QtCore.QTimer()
    QtCore.QObject.connect(self.updateTimer, QtCore.SIGNAL("timeout()"), self.setToolTip)
    self.updateTimer.start(1000)
    self.stateList = [{'State': 'success', 'Color': '#00ff00'},
                      {'State': 'failed' , 'Color': '#ff0000'}]

  def showMenu(self, reason):
    if reason == QtGui.QSystemTrayIcon.Context:
      self.menu.show()


  def setToolTip(self):
    toolTip = """<table style='cellspacing:100px'><tr style='font-weight:bold'>
                 <td>Name</td><td>Status</td><td>Last Build</td></tr><tbody>"""
    oneFailed = 0
    oneBuilding = 0
    for builder in self.client.listener.builders:
      toolTip += "<tr><td>" + builder["Name"] + "</td>"
      toolTip += "<td>" + builder["State"] + "</td>"
      res = builder["LastResult"] 
      if res != 0:
        oneFailed = 1
      if builder["State"] == "building":
        oneBuilding = 1
      toolTip += "<td style='background-color:%s'>%s</td></tr>" % (self.stateList[res]["Color"], self.stateList[res]["State"])
    
    toolTip += "</tbody></table>"
    self.trayIcon.setToolTip(toolTip)

    if oneBuilding:
      self.trayIcon.setIcon(self.buildingIcon)
    elif oneFailed:
      self.trayIcon.setIcon(self.failedIcon)
    else:
      self.trayIcon.setIcon(self.okIcon)

  def quit(self):
    self.client.stop()
    self.client.join()
    sys.exit(0)

if __name__ == '__main__':
  app = QtGui.QApplication(sys.argv)
  applet = BuildbotApplet()
  sys.exit(app.exec_())

